package com.example.robolizardoficial;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Inicio extends AppCompatActivity {

    Button btnIngresar, btnValidarToken,btnGenerarToken;
    TextView horaActual;
    LinearLayout linear1,linear2;
    Animation uptodown,downtoup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        btnIngresar =findViewById(R.id.ingresarApp);
        btnValidarToken =findViewById(R.id.validarToken);
        btnGenerarToken =findViewById(R.id.generarToken);
        btnGenerarToken.setVisibility(View.GONE);
        horaActual=findViewById(R.id.horaActual);
        linear1=findViewById(R.id.linear1);
        linear2=findViewById(R.id.linear2);
        uptodown= AnimationUtils.loadAnimation(this,R.anim.uptodown);
        downtoup= AnimationUtils.loadAnimation(this,R.anim.downtoup);
        linear1.setAnimation(uptodown);
        linear2.setAnimation(downtoup);

        iSender2.sendEmptyMessage(0);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }





    public void verificartoken(View v) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                String url ="http://lizardrobot.ga/prueba/";
                //url = url + "accion=" + "1";

                JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST,url,null,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    String valor = response.getJSONObject(0).getString("status");
                                    if (valor.equals("false")) {
                                        Toast.makeText(getApplicationContext(), "Token vigente", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Token Expirado", Toast.LENGTH_LONG).show();
                                        btnGenerarToken.setVisibility(View.VISIBLE);
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(),
                                            "Data Error",
                                            Toast.LENGTH_LONG).show();

                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe que tiene acceso a internet",
                                Toast.LENGTH_LONG).show();

                    }
                })
                {    /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences datos = getSharedPreferences("Data", Context.MODE_PRIVATE);
                    String token = datos.getString("token","nel");
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Authorization", "Bearer " + token);
                    return headers;
                }
                };
                queue.add(stringRequest);
            };
        });
    }

    public void renovartoken(View v) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                String url ="http://lizardrobot.ga/o/token/";


                StringRequest stringRequest = new StringRequest(Request.Method.POST,url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject test = new JSONObject(response);
                                    Toast.makeText(getApplicationContext(), test.getString("access_token"), Toast.LENGTH_LONG).show();
                                    SharedPreferences datos = getSharedPreferences("Data", Context.MODE_PRIVATE);

                                    SharedPreferences.Editor editor = datos.edit();
                                    editor.putString("token",test.getString("access_token"));

                                    editor.commit();
                                    btnGenerarToken.setVisibility(View.GONE);
                                } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(),
                                            "Data Error",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe que tiene acceso a internet",
                                Toast.LENGTH_LONG).show();
                    }
                })
                {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    return headers;
                }
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("grant_type", "client_credentials");
                        params.put("client_id", "YJgZq0WL3QFIpzNFQ2vO8k0zmKUkXuASmwPCYc4a");
                        params.put("client_secret", "Dg33kBb9EymDUCbkMvZXZMMUu1zd3or878A2tbbFlFyahMXGLSVJZwljaoUpZLawEocZLkoZ3jxb6hiFFgC7CuAzyTIwsbi47OeDhFj9vTGv32NEL61aTxrsYikQqIZR");
                        params.put("scope", "android");
                        return params;
                    }
                };
                queue.add(stringRequest);
            };
        });
    }

    public void horaActual() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        String time = "[ "+format.format(calendar.getTime())+" ]";
        horaActual.setText(time);

    }


    //------Handler peticion cada 5 secs-----------//
    private Handler iSender2 = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            horaActual();
            iSender2.sendEmptyMessageDelayed(0, 1000);
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        iSender2.removeMessages(0);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        iSender2.sendEmptyMessage(0);
        uptodown= AnimationUtils.loadAnimation(this,R.anim.uptodown);
        downtoup= AnimationUtils.loadAnimation(this,R.anim.downtoup);
        linear1.setAnimation(uptodown);
        linear2.setAnimation(downtoup);
    }
}
