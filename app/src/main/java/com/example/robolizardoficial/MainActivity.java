package com.example.robolizardoficial;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    TextView txtTemperatura, txtHumedad,txtEstadoRobot;
    Button generar;
    LinearLayout linear3;
    Animation downtoup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //lbltexto = findViewById(R.id.lbltexto);
        //generar = findViewById(R.id.generar);
        //generar.setVisibility(View.GONE);
        txtTemperatura = findViewById(R.id.txtTemperatura);
        txtHumedad = findViewById(R.id.txtHumedad);
        txtEstadoRobot=findViewById(R.id.txtEstadoRobot);

        linear3=findViewById(R.id.linear3);
        downtoup= AnimationUtils.loadAnimation(this,R.anim.downtoup);
        linear3.setAnimation(downtoup);


        TomarMedicionMovimiento();

        //------Handler peticion cada 5 secs------ON CREATE-------------------------------//
        iSender.sendEmptyMessage(0);
    }

    private Handler iSender = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            TomarMedicion();
            TomarMedicionMovimiento();
            iSender.sendEmptyMessageDelayed(0, 5*1000);
        }
    };
        //--------Parar el Handler en caso se cambie de VIEW----------------------------///////
    @Override
    protected void onStop() {
        super.onStop();
        //iSender.removeCallbacks(AsyncTask);}
        iSender.removeMessages(0);
    }
    //---------------Metodo de notificacion--------------//

    public void agregarNotificacion(){
        Uri alarm= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.iconhumedad)
                .setContentTitle("Humedad en el ambiente")
                .setSound(alarm)
                .setStyle(new NotificationCompat.BigTextStyle()
                .bigText("Se han detectado valores anormales de temperatura, favor revisar la aplicacion para obtener mas detalles sobre el problema."));

                //.setContentText("Detectado valor de humedad excesivo.");

        Intent notificacionIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificacionIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(contentIntent);
        long[] vibraPattern = { 1000, 1000, 1000, 1000,1000,1000 };
        builder.setVibrate(vibraPattern);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1,builder.build());
    }

/*
    public void callAsynchronousTask() {
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {

                            PerformBackgroundTask performBackgroundTask = new PerformBackgroundTask();
                            // PerformBackgroundTask this class is the class that extends AsynchTask
                            performBackgroundTask.execute();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 50000); //execute in every 50000 ms
    }
*/

    //------------------------------------------------------------------------BOTONES------------------------------------------------------------//

    public void IrAdelante(View v) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                String url ="http://lizardrobot.ga/send-accion/?";
                url = url + "accion=" + "0";

                JsonArrayRequest stringRequest = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    String valor = response.getJSONObject(0).getString("Mensaje");
                                    if (valor.equals("Update Success")) {
                                        Toast.makeText(getApplicationContext(), "Peticion Exitosa", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Credenciales invalidas", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(),
                                            "No es array",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Token Expirado",
                                Toast.LENGTH_LONG).show();

                    }
                })
                { /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences datos = getSharedPreferences("Data",Context.MODE_PRIVATE);
                    String token = datos.getString("token","nel");
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Authorization", "Bearer " + token);
                    return headers;
                }
                };
                queue.add(stringRequest);
            };
        });
    }
    //-----------------------------------------------------------------------------------------------------------------------------------------//
    public void retroceder(View v) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                String url ="http://lizardrobot.ga/send-accion/?";
                url = url + "accion=" + "1";

                JsonArrayRequest stringRequest = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    String valor = response.getJSONObject(0).getString("Mensaje");

                                    if (valor.equals("Update Success")) {
                                        Toast.makeText(getApplicationContext(), "Peticion Exitosa", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Credenciales invalidas", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(),
                                            "No es array",
                                            Toast.LENGTH_LONG).show();

                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Token Expirado",
                                Toast.LENGTH_LONG).show();

                    }
                })
                {/**
                 * Passing some request headers
                 */

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences datos = getSharedPreferences("Data",Context.MODE_PRIVATE);
                    String token = datos.getString("token","nel");
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Authorization", "Bearer " + token);
                    return headers;
                }
                };
                queue.add(stringRequest);
            };
        });
    }
    //-----------------------------------------------------------------------------------------------------------------------------------------//
    public void detenerse(View v) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                String url ="http://lizardrobot.ga/send-accion/?";
                url = url + "accion=" + "2";

                JsonArrayRequest stringRequest = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    String valor = response.getJSONObject(0).getString("Mensaje");

                                    if (valor.equals("Update Success")) {
                                        Toast.makeText(getApplicationContext(), "Peticion Exitosa", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Credenciales invalidas", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(),
                                            "No es array",
                                            Toast.LENGTH_LONG).show();

                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Token Expirado",
                                Toast.LENGTH_LONG).show();

                    }
                })
                {/**
                 * Passing some request headers
                 */

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences datos = getSharedPreferences("Data",Context.MODE_PRIVATE);
                    String token = datos.getString("token","nel");
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Authorization", "Bearer " + token);
                    return headers;
                }
                };
                queue.add(stringRequest);
            };
        });
    }

    //------------------------------------------------------------------------FIN------------------------------------------------------------//


    public void TomarMedicion(View v){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                String url ="http://lizardrobot.ga/datos/";
                JsonArrayRequest stringRequest = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    String temperatura = response.getJSONObject(0).getString("temperatura");
                                    String humedad = response.getJSONObject(0).getString("humedad");
                                    String valor = response.getJSONObject(0).getString("id");
                                    if(valor.equals("2")){
                                        txtHumedad.setText("  "+humedad+" %");
                                        txtTemperatura.setText("  "+temperatura + " °C");
                                        Toast.makeText(getApplicationContext(),"Lectura tomada con exito.",Toast.LENGTH_LONG).show();
                                    }else{
                                        Toast.makeText(getApplicationContext(),"Credenciales invalidas",Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(), "Error en la data recibida", Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Compruebe que tiene acceso a internet", Toast.LENGTH_LONG).show();
                    }
                });
                queue.add(stringRequest);
            }
        });
    }


    public void TomarMedicion(){

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                String url ="http://lizardrobot.ga/datos/";
                JsonArrayRequest stringRequest = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    String temperatura = response.getJSONObject(0).getString("temperatura");
                                    String humedad = response.getJSONObject(0).getString("humedad");
                                    txtHumedad.setText("  "+humedad+" %");
                                    txtTemperatura.setText("  "+temperatura + " °C");
                                       if(Double.parseDouble(humedad)>30){
                                            agregarNotificacion();
                                        }else{
                                            Toast.makeText(getApplicationContext(),"El valor es menor a 30.",Toast.LENGTH_LONG).show();
                                        }
                                } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(),
                                            "Error en la data recibida",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe que tiene acceso a internet",
                                Toast.LENGTH_LONG).show();
                    }
                });
                queue.add(stringRequest);
            }
        });
    }




    public void TomarMedicionMovimiento(){

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                String url ="http://lizardrobot.ga/acciones/";
                JsonArrayRequest stringRequest = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    String accion = response.getJSONObject(0).getString("accion");


                                    //Toast.makeText(getApplicationContext(),"Lectura tomada con exito.",Toast.LENGTH_LONG).show();
                                    if(Integer.parseInt(accion)==2){
                                        txtEstadoRobot.setText("Detenido");
                                    }else if(Integer.parseInt(accion)==1){
                                        txtEstadoRobot.setText("Retrocediendo...");
                                    }else if(Integer.parseInt(accion)==0){
                                        txtEstadoRobot.setText("Avanzando...");
                                    }else{
                                        Toast.makeText(getApplicationContext(),"No se reconoce la accion.",Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(),
                                            "Error en la data recibida",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe que tiene acceso a internet",
                                Toast.LENGTH_LONG).show();
                    }
                });
                queue.add(stringRequest);
            }
        });
    }








}


