package com.example.robolizardoficial;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class logear extends AppCompatActivity {

    EditText txtUsuario, txtClave;
    Button btnLogear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logear);

        txtUsuario = findViewById(R.id.txtUsuario);
        txtClave = findViewById(R.id.txtClave);
        btnLogear = findViewById(R.id.btnLogear);
    }



    public void Logear(View v) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                String url ="http://192.168.0.29:8000/login/";


                StringRequest stringRequest = new StringRequest(Request.Method.POST,url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONArray test = new JSONArray(response);
                                    String valor = test.getJSONObject(0).getString("login");
                                    if (valor.equals("true")) {
                                        String nombre = txtUsuario.getText().toString().trim();
                                        String clave = txtClave.getText().toString().trim();
                                        SharedPreferences datos = getSharedPreferences(nombre, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = datos.edit();
                                        editor.putString("nombre", nombre);
                                        editor.putString("clave", clave);
                                        editor.apply();
                                        Intent llamar = new Intent(getApplicationContext(), Inicio.class);
                                        llamar.putExtra("nombre",nombre);
                                        startActivity(llamar);
                                        finish();
                                        //Toast.makeText(getApplicationContext(), "Credenciales correctas", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Credenciales invalidas", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(getApplicationContext(),
                                            "Error en la data recibida",
                                            Toast.LENGTH_LONG).show();

                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe que tiene acceso a internet",
                                Toast.LENGTH_LONG).show();

                    }
                })
                {    /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");

                    return headers;
                }

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String>  params = new HashMap<String, String>();
                        String nombre = txtUsuario.getText().toString().trim();
                        String clave = txtClave.getText().toString().trim();
                        params.put("nombre", nombre);
                        params.put("clave", clave);

                        return params;
                    }
                };
                queue.add(stringRequest);
            };
        });
    }


}
